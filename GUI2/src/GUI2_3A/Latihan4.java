package GUI2_3A;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Latihan4 extends JFrame {

    public Latihan4() {
        this.setLayout(null);
        this.setSize(350, 150);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);

        JLabel label = new JLabel("KEYWORD : ");
        label.setBounds(130, 12, 90, 20);
        this.add(label);

        JTextField textfield = new JTextField(null);
        textfield.setBounds(20, 40, 290, 20);
        this.add(textfield);

        JButton button = new JButton("Find");
        button.setBounds(116, 80, 90, 20);
        this.add(button);

    }

    public static void main(String[] args) {
        new Latihan4();
    }
}
